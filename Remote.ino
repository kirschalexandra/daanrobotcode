
////////////////////////////////////////////////////////////////////////////////////

boolean rcValid(){
  boolean ch1valid = ((rc_values[RC_CH1] > 0) && (rc_values[RC_CH1] < 2200));
  boolean ch2valid = ((rc_values[RC_CH2] > 0) && (rc_values[RC_CH2] < 2200));
  boolean ch3valid = ((rc_values[RC_CH3] > 0) && (rc_values[RC_CH3] < 2200));
  boolean ch4valid = ((rc_values[RC_CH4] > 0) && (rc_values[RC_CH4] < 2200));
  return (ch1valid && ch2valid && ch3valid && ch4valid);
}

void resetRcValues(){
  noInterrupts();

  rc_shared[RC_CH1] = 0;
  rc_shared[RC_CH2] = 0;
  rc_shared[RC_CH3] = 0;
  rc_shared[RC_CH4] = 0;
  
  rc_values[RC_CH1] = 0;
  rc_values[RC_CH2] = 0;
  rc_values[RC_CH3] = 0;
  rc_values[RC_CH4] = 0;
  
  interrupts();
  }

void processRemote() {
    int speedScale = map(rc_values[RC_CH2],1984,980,0,128);
    speedScale = max(0, speedScale);
    speedScale = min(128, speedScale);

    // remove noise around the min and max values
    int totalSpeed = map(rc_values[RC_CH1], 1000, 1992, (speedScale * -1), speedScale);
    totalSpeed = max(-128, totalSpeed);
    totalSpeed = min(127, totalSpeed);

    // remove noise around position zero 
    if (abs(totalSpeed) < 5) {
      totalSpeed = 0;
    }


    // remove noise around the min and max values; turn down speed for turning
    int turnSpeed = map(rc_values[RC_CH3], 972, 1972, int((speedScale * -0.5)), int((speedScale * 0.5)));
    turnSpeed = max(-128, turnSpeed);
    turnSpeed = min(127, turnSpeed);

    // remove noise around position zero 
    if (abs(turnSpeed) < 5) {
      turnSpeed = 0;
    }

    int projectorTargetTemp = map(rc_values[RC_CH4],2000,1000,0,180);
    projectorTargetTemp = max(0, projectorTargetTemp);
    projectorTargetTemp = min(180, projectorTargetTemp);
    projectorTarget = projectorTargetTemp;

    if (useHPS) {
      rcPercepts["rc-forward-raw"] = rc_values[RC_CH1];
      rcPercepts["rc-speed-scale-raw"] = rc_values[RC_CH2];
      rcPercepts["rc-turn-raw"] = rc_values[RC_CH3];
      rcPercepts["rc-projector-pos-raw"] = rc_values[RC_CH4];
      rcPercepts["rc-forward"] = totalSpeed;
      rcPercepts["rc-speed-scale"] = speedScale;
      rcPercepts["rc-turn"] = turnSpeed;
      rcPercepts["rc-projector-pos"] = projectorTargetTemp;
      rcPercepts.printTo(Serial);
      Serial.println();
    }
    else {
      mdSetVelocities(totalSpeed, turnSpeed);
      Serial.print("RC_CH1: ");
      Serial.println(rc_values[RC_CH1]);
      Serial.print("RC_CH2: ");
      Serial.println(rc_values[RC_CH2]);
      Serial.print("RC_CH3: ");
      Serial.println(rc_values[RC_CH3]);
      Serial.print("RC_CH4: ");
      Serial.println(rc_values[RC_CH4]);
      Serial.print("turn: ");
      Serial.println(turnSpeed);
      Serial.print("total: ");
      Serial.println(totalSpeed);
      Serial.print("scale: ");
      Serial.println(speedScale);
      Serial.println("---");
    }
    
    

}

